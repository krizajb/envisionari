package envisionari.controllers;

import envisionari.beans.UsersBean;
import envisionari.models.Swsusers;
import org.json.JSONObject;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

@RequestScoped
@ManagedBean
public class UsersController {

    @EJB
    private UsersBean users;
    // Session parameters
    private Long id;
    private String username;
    private String password;
    private String result;

    @PostConstruct
    public void init() {
    }

    public void add() {
        Swsusers user = new Swsusers(this.id, this.username, this.password);
        JSONObject result = this.users.createUser(user);
        if (result.has("error")) {
            this.result = result.getString("error");
        }
        else {
            this.result = String.format("User '%s' created", this.username);
        }
    }

    public void remove(Swsusers user) {
        this.users.deleteUser(user);
        this.result = String.format("User '%s' removed", user.getUsername());
    }

    public List<Swsusers> getList() {
        return this.users.getUsers();
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
