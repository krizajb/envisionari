package envisionari.beans;

import envisionari.models.Swsusers;

import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;

@Stateless(name = "UsersEJB")
@ManagedBean
public class UsersBean implements Serializable {

    @PersistenceContext(unitName="kazsw-persistence-unit")
    private EntityManager em;
    private final static Logger LOG = Logger.getLogger(UsersBean.class);


    public JSONObject createUser(Swsusers user) {
        LOG.info(String.format("Creating user '%s'", user.getUsername()));

        JSONObject result = new JSONObject();
        if (user.getUsername().isEmpty()) {
            result.put("error", "Required field 'name' missing");
        }
        else {
            result.put("name", user.getUsername());

            if (!this.exists(Swsusers.class, "username", user.getUsername())) {
                this.em.persist(user);
            }
            else {
                result.put("error", "Unique username required");
                LOG.error(String.format("Unique username required '%s'", user.getUsername()));
            }
        }

        return result;
    }

    public JSONObject deleteUser(String name) {
        JSONObject result = new JSONObject();
        result.put("name", name);
        this.deleteUser(em.find(Swsusers.class, name));

        return result;
    }

    public void deleteUser(Swsusers user) {
        LOG.info(String.format("Removing user '%s'",user.getUsername()));
        this.em.remove(this.em.contains(user) ? user : this.em.merge(user));
    }

    public List<Swsusers> getUsers() {
        final CriteriaQuery<Swsusers> criteria = em.getCriteriaBuilder().createQuery(Swsusers.class);
        criteria.select(criteria.from(Swsusers.class));

        return this.em.createQuery(criteria).getResultList();
    }

    private boolean exists(String name) {
        final String query = "select count(e) from Swsusers e where username='"+name+"'";
        // you will always get a single result
        Long count = (Long) this.em.createQuery( query ).getSingleResult();
        return count.equals(0L);
    }

    private <E> boolean exists(final Class<E> entityClass, final String property, final String key) {
        final CriteriaBuilder cb = this.em.getCriteriaBuilder();

        final CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        final Root<E> from = cq.from(entityClass);

        cq.select(cb.count(from));
        cq.where(cb.equal(from.get(property), key));

        final TypedQuery<Long> tq = this.em.createQuery(cq);
        return tq.getSingleResult() > 0;
    }

}
