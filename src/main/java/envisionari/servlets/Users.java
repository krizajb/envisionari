package envisionari.servlets;

import envisionari.beans.UsersBean;
import envisionari.models.Swsusers;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.ejb.EJB;
import javax.servlet.http.HttpServlet;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Users HttpServlet handling following set of post actions:
 * - users-add
 * - users-remove
 * - users-edit
 *
 * Servlet development/use was discontinued with the introduction of ajax.
 */
@WebServlet(urlPatterns = {"/users-add", "/users-remove", "/users-edit"})
public class Users extends HttpServlet {

    private enum Method {
        ADD_USER     ("/users-add"),
        REMOVER_USER ("/users-remove"),
        EDIT_USER    ("/users-edit");

        final String name;
        Method(String name) {
            this.name = name;
        }

        static Method getMethod(String name) {
            Method method = null;
            for (Method m : values()) {
                if (m.name.toUpperCase().equals(name.toUpperCase())) {
                    method = m;
                }
            }

            return method;
        }
    }

    @EJB
    UsersBean users;
    private final static Logger LOG = Logger.getLogger(Users.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        LOG.info(String.format("Processing Users post request '%s'", request.getServletPath()));

        Long id = Long.parseLong(request.getParameter("id"));
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        Method method = Method.getMethod(request.getServletPath());
        JSONObject responseJson = new JSONObject();

        switch (method) {
            case ADD_USER: {
                responseJson = this.users.createUser(new Swsusers(id, name, password));
                break;
            }
            case REMOVER_USER: {
                responseJson = this.users.deleteUser(name);
                break;
            }
            default:
                responseJson.append("error", "unsupported user action");
        }

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(responseJson.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

    }
}
