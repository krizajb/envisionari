package envisionari.models;

import javax.persistence.*;

@Entity
public class Swsusers {

    private Long id;
    private String username;
    private String passwd;

    public Swsusers() {
    }

    public Swsusers(Long id, String name, String password) {
        this.id = id;
        this.username = name;
        this.passwd = password;
    }

    @Basic
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "USERNAME")
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "PASSWD")
    public String getPasswd() {
        return this.passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Swsusers swsusers = (Swsusers) o;

        if (this.id != null ? !id.equals(swsusers.id) : swsusers.id != null) return false;
        if (this.username != null ? !this.username.equals(swsusers.username) : swsusers.username != null)
            return false;
        if (this.passwd != null ? !this.passwd.equals(swsusers.passwd) : swsusers.passwd != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = this.id != null ? this.id.hashCode() : 0;
        result = 31 * result + (this.username != null ? this.username.hashCode() : 0);
        result = 31 * result + (this.passwd != null ? this.passwd.hashCode() : 0);
        return result;
    }
}
